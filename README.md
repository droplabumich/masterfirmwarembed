#Main Sphere Control Board Firmware

This repository contains the firmware that runs on the LPC1768 microcontroller and interfaces sensors, thrusters and the ODROID computer. 
For more information visit [www.dropSphere.net](http://www.dropsphere.net)

##To compile for the custom board: 

mbed compile -t GCC_ARM  -m LPC1768 -DTARGET_BOARD -c


##To compile for the breadboard system: 

mbed compile -t GCC_ARM  -m LPC1768 -DDEV_BOARD -c

##Dependencies: 

The following libraries are required: 
1. GPS-Library
2. HIH6130
3. ESC
4. FPointer
5. MPL3115A2
6. Sensor_data
7. SerialCommandInterface 

The libraries are all located in a common folder to the sphere project. Symbolic links are placed inside this project to link to the libraries. 

## To copy to breadboard 

rm /media/eiscar/MBED/MasterFirmwareMbed.bin
cp ./BUILD/LPC1768/GCC_ARM/MasterFirmwareMbed.bin /media/eiscar/MBED/


## To commit in Mercurial: hg commit  --config ui.username=eiscar  -m "Added files and changed configs etc" 

## Copy to vehicle (on university local network)

scp ./BUILD/LPC1768/GCC_ARM/MasterFirmwareMbed.bin odroid@141.212.194.156:/home/odroid/odroid-development/lpc_1768_isp-programmer/binaries/

## Upload to board 

1. Login over ssh to the sphere 
2. Run python /home/odroid/odroid-development/lpc_1768_isp-programmer/scripts/lpc1768-prog.py
3. You should see the microcontroller device id being read and the flashing process begins. This can take a few minutes depending on the size of the binary. 

## Check Serial Stream comming in

1. Type gpio mode 3 out 
		gpio mode 22 out 
		gpio write 22 0 
		gpio write 3 0  
1. Open minicom -s 
2. Go to Serial port setup, choose port /dev/ttySAC0 and set the speed to 115200. Set flow control to no
3. Exit and you should see the incoming serial stream. 

## To connect the exterior serial to the LPC
1. Type gpio mode 26 out 
		gpio write 26 1

# Sensors 

1. INA219 Current Sensor 1, connected to Bus Voltage: A1=3.3V, A0=3.3V  -> I2C Address: 1000101 0x45
2. INA219 Current Sensor 2, connected to 3.3V rail: A1=3.3V, A0=0V		-> I2C Address: 1000100 0x44
3. INA219 Current Sensor 3, connected to 5V rail: A1=0V, A0=3.3V 		-> I2C Address: 1000001 0x41
4. INA219 Current Sensor 4, connected to cameras 12V rail: A1=SDA, A0=SDA 		-> I2C Address: 1001010 0x4A
5. INA219 Current Sensor 5, connected to altimeter 12V rail: A1=0V, A0=0V 		-> I2C Address: 1000000 0x40
4. MPL


# Debugging information
 - The reset line is present at pin 


