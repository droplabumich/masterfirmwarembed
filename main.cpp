// Copyright 2017, Eduardo Iscar

#include "mbed-os/mbed.h"


#include "pinDefs.h"
#include "tests.h"
#include "status.h"

#include "EM506/EM506_GPS.h"
#include "ESC/esc.h"
#include "External-Pressure/ExtPressure.h"
#include "HIH6130/HIH6130.h"
#include "INA219/INA219.h"
#include "MPL3115A2/MPL3115A2.h"
#include "Sensor_data/Sensor_data.h"
#include "SerialCommandInterface/CommandInterface.h"
#include "VA500P/include/va500-mbed.h"
#include "XsensImuMbed.h"
#include "Watchdog/Watchdog.h"


#define IMU_CAMERA_FREQ_DIVIDER 5

// ****************** Physical Interfaces  *********************

I2C i2c(I2C_SDA, I2C_SCL);          // sda, scl
RawSerial pc(PC_TX, PC_RX, 115200); // tx, rx
RawSerial gps_serial(GPS_TX, GPS_RX, 4800);
RawSerial imu_serial(XSENS_TX, XSENS_RX, 115200);

#ifdef TARGET_BOARD
RawSerial va500_serial(EXT_TX, EXT_RX, 115200);
#endif 

// ****************** Global variables      *******************

SENSOR_DATA::State state;
volatile bool signal_imu = false;  
volatile double oldiface; 
volatile int ms_counter = 0;
uint64_t program_status = 0;  // Used to indicate the state the program is in. 
uint64_t* status = &program_status;

// ******************  Sensor Objects  *************************

MPL3115A2 int_press_sensor(&i2c); 
HIH6130 hih6130(&i2c, HIH6130_I2C_ADDRESS);
EM506_GPS gps(&gps_serial);
XsensImuMbed imu(&imu_serial);

#ifdef TARGET_BOARD
mbedVA500P va500(&va500_serial, RangeUnits::meters, PressureUnits::dBar);
#endif

INA219 curr5v(i2c, &ina219_5V_paramtr);
INA219 curr3v(i2c, &ina219_3V_paramtr);
INA219 currBatv(i2c, &ina219_BatV_paramtr);
INA219 curr12VCam(i2c, &ina219_12VCam_paramtr);
INA219 curr12VAlt(i2c, &ina219_12VAlt_paramtr);


ESC esc_ul(PWM_UL_PIN, true);
ESC esc_ur(PWM_UR_PIN, true);
ESC esc_fl(PWM_FL_PIN);
ESC esc_fr(PWM_FR_PIN);


// ******************* Serial Interface ***********************
SerialProtocol protocol(&state);

// ******************    GPIO pins      ***********************
DigitalOut heartbeat_led(LED1_p);
DigitalOut myled2(LED2_p);
DigitalOut debug_pin(DEBUG_PIN);
DigitalOut camera_trigger_pin(CS_PIN, 0);

// ******************    Threads        ***********************
Thread sensor_thread(osPriorityNormal, 3072, NULL, "sensor_thread");
Thread imu_thread(osPriorityRealtime, 3072, NULL, "imu_thread");

Mutex i2c_mutex;
Mutex pc_serial_mutex;

// ******************    Timers         ***********************
Timeout thruster_brownout;  // Timeout to stop the thrusters in case no new command data arrives
Timeout va500_noData;       // Timeout to notify when no altimeter data being received
Timeout gps_noData;         // Timeout to notify when no gps data is being received
Timeout imu_noData;         // Timeout to notify when no IMU data is being received


Timeout cameraTriggerReset; // Timeout to reset the camera trigger pin 
bool cameraTriggerEnabled = false;


Watchdog wdt; // Setup the watchdog timer

Ticker ms_ticker; // Ticker to count time with ms precision 

char *sendAD(int *message_length);

// ******************    Functions         ***********************


bool updateBLE() {
    // Send info to bluetooth transmitter
    //i2c_mutex.lock();
    int message_length;
    char *message = protocol.createmsg(SerialProtocol::DM, state.sensors[DATA_TYPES::r3V_voltage].id, state.sensors[DATA_TYPES::r3V_voltage].value, &message_length);
    bool result_1, result_2, result_3, result_4, result_5, result_6, result_7, result_8;
    //pc.printf("Length: %i Message: %s", message_length, message);
    result_1 = i2c.write(BLE_NANO_ADDRESS << 1, message, message_length - 1);
    result_2 = i2c.write(BLE_NANO_ADDRESS << 1, "\n\r", 2);
    ThisThread::sleep_for(1);
    message = protocol.createmsg(SerialProtocol::DM, state.sensors[DATA_TYPES::humidity].id, state.sensors[DATA_TYPES::humidity].value, &message_length);
    result_3 = i2c.write(BLE_NANO_ADDRESS << 1, message, message_length - 1);
    result_4 = i2c.write(BLE_NANO_ADDRESS << 1, "\n\r", 2);
    ThisThread::sleep_for(1);
    message = protocol.createmsg(SerialProtocol::DM, state.sensors[DATA_TYPES::press_int].id, state.sensors[DATA_TYPES::press_int].value, &message_length);
    result_5 = i2c.write(BLE_NANO_ADDRESS << 1, message, message_length - 1);
    result_6 = i2c.write(BLE_NANO_ADDRESS << 1, "\n\r", 2);
    ThisThread::sleep_for(1);
    message = protocol.createmsg(SerialProtocol::DM, state.sensors[DATA_TYPES::temp_int_mpl].id, state.sensors[DATA_TYPES::temp_int_mpl].value, &message_length);
    result_7 = i2c.write(BLE_NANO_ADDRESS << 1, message, message_length - 1);
    result_8 = i2c.write(BLE_NANO_ADDRESS << 1, "\n\r", 2);
    ThisThread::sleep_for(1);
    //i2c_mutex.unlock();
    return (result_1 && result_2 && result_3 && result_4 && result_5 && result_6 && result_7 && result_8);
}

void noVa500Data() {
  pc.printf("No VA500 data received\n\r");
  setBit(status, VA500_NODATA_ERROR); // Set VA500 No data error flag
  va500_noData.attach(&noVa500Data, 2);
}

void noImuData() {
  pc.printf("No IMU data received\n\r");
  setBit(status, IMU_ERROR); // Set IMU error flag
  imu_noData.attach(&noImuData, 2);
}

void noGpsData() {
  pc.printf("No Gps data received\n\r");
  setBit(status, GPS_ERROR); // Set GPS error flag 
  gps_noData.attach(&noGpsData, 4);
}

void initialize_thrusters() {
  state.sensors[DATA_TYPES::pwm_ul].value = 0.5;
  state.sensors[DATA_TYPES::pwm_ur].value = 0.5;
  state.sensors[DATA_TYPES::pwm_fl].value = 0.5;
  state.sensors[DATA_TYPES::pwm_fr].value = 0.5;
  esc_ul.setThrottle(state.sensors[DATA_TYPES::pwm_ul].value);
  esc_ur.setThrottle(state.sensors[DATA_TYPES::pwm_ur].value);
  esc_fl.setThrottle(state.sensors[DATA_TYPES::pwm_fl].value);
  esc_fr.setThrottle(state.sensors[DATA_TYPES::pwm_fr].value);
  esc_ul.pulse();
  esc_ur.pulse();
  esc_fl.pulse();
  esc_fr.pulse();
}

void disableThrusters() {
  initialize_thrusters();
  // Set state to blinking red
  setBit(status, NO_ST_DATA);
}

void reportErrors() {
  pc_serial_mutex.lock();
  for (int j=0; j<64; j++){
    if (checkError(status, j)) {
      pc.printf("Byte %i set\n\r", j);
    }
  }
  pc_serial_mutex.unlock();
}

void sensor_polling() {
  int mpl_reconnection_attempts = 0;
  int success_hih, success_mpl, error_gps = 0;
  ThisThread::sleep_for(500); // Wait 0.5sec to allow sensors to boot up.

  // Initialize GPS
  gps.sendCommand(SET_GSA_OFF);
  gps.sendCommand(SET_RMC_OFF);
  gps.sendCommand(SET_GSV_OFF);
  gps_noData.attach(&noGpsData,4);

  // Initialize MPL Pressure - Temperature sensor 
  int_press_sensor.init();
  int_press_sensor.setOffsetTemperature(20); // Offsets for Dacula, GA
  int_press_sensor.setOffsetPressure(127);

  pc_serial_mutex.lock();
  pc.printf("** MPL3115A2 SENSOR **\r\n");
  pc.printf("Who Am I: 0x%X\r\n", int_press_sensor.whoAmI());
  pc.printf("OFF_H: 0x%X, OFF_T: 0x%X, OFF_P: 0x%X\n\r",
            int_press_sensor.offsetAltitude(),
            int_press_sensor.offsetTemperature(),
            int_press_sensor.offsetPressure());
  pc_serial_mutex.unlock();

  // Transmission headers
  const int id_data_array_packet_1[12] = {
    DATA_TYPES::temp_int_hih,     // In degree Celsius
    DATA_TYPES::temp_int_mpl,     // In degree Celsius
    DATA_TYPES::temp_int_xsens,   // In degree Celsius
    DATA_TYPES::temp_ext,         // In degree Celsius
    DATA_TYPES::press_int,        // In Pascals
    DATA_TYPES::humidity,         // In percent
    DATA_TYPES::status,           // Int
    DATA_TYPES::pwm_ul,           // Setpoint, between 0 and 1
    DATA_TYPES::pwm_ur,           // Setpoint, between 0 and 1
    DATA_TYPES::pwm_fr,           // Setpoint, between 0 and 1
    DATA_TYPES::pwm_fl,           // Setpoint, between 0 and 1
    DATA_TYPES::user_iface,       // Int
  };

  const int id_data_array_packet_2[12] = {
    DATA_TYPES::current_3V,       // In mA
    DATA_TYPES::current_5V,       // In mA
    DATA_TYPES::current_bat,      // In mA
    DATA_TYPES::rbat_voltage,     // In V
    DATA_TYPES::r5V_voltage,      // In V
    DATA_TYPES::r3V_voltage,      // In V
    DATA_TYPES::latitude,         // In degrees
    DATA_TYPES::longitude,        // In degrees
    DATA_TYPES::cam_voltage,      // In V
    DATA_TYPES::altimeter_voltage,// In V
    DATA_TYPES::cam_current,      // In mA
    DATA_TYPES::altimeter_current,// In mA
  };

  double data_array[12];

  while (1) {
    
    // Read all sensor data
    
    // ************ Read HIH Temp-Humidity sensor ********************// 
    success_hih = hih6130.ReadData(&state.sensors[DATA_TYPES::temp_int_hih].value,
                                 &state.sensors[DATA_TYPES::humidity].value);

    if (!success_hih) {
      setBit(status, HIH_ERROR);
    } else {
      clearBit(status, HIH_ERROR);
    }

    // ************ Read MPL Temp-Pressure sensor ********************// 
    success_mpl = int_press_sensor.readData(
      &state.sensors[DATA_TYPES::temp_int_mpl].value,
      &state.sensors[DATA_TYPES::press_int].value, Temperature::CELSIUS,
      Pressure::PASCALS);
    
    if (!success_mpl) {
      setBit(status, MPL_ERROR);
    } else {
      clearBit(status, MPL_ERROR);
    }


    // ****************** Read GPS sensor ***************************// 
    
    if (gps.newNMEAreceived()) {
      clearBit(status, GPS_ERROR);  
      if (gps.parse(gps.lastNMEA())) {
        if (gps.lat == 'S') {
          state.sensors[DATA_TYPES::latitude].value = -gps.latitude;
        } else if (gps.lat == 'N') {
          state.sensors[DATA_TYPES::latitude].value = gps.latitude;
        }
        if (gps.lon == 'W') {
          state.sensors[DATA_TYPES::longitude].value = -gps.longitude;
        } else if (gps.lon == 'E') {
          state.sensors[DATA_TYPES::longitude].value = gps.longitude;
        }
      }
    } 
      
    // ************  Transmit data to Odroid *************************//

    state.sensors[DATA_TYPES::r5V_voltage].value = curr5v.read_bus_voltage();
    state.sensors[DATA_TYPES::r3V_voltage].value = curr3v.read_bus_voltage();
    state.sensors[DATA_TYPES::rbat_voltage].value = currBatv.read_bus_voltage();
    state.sensors[DATA_TYPES::cam_voltage].value = curr12VCam.read_bus_voltage();
    state.sensors[DATA_TYPES::altimeter_voltage].value = curr12VAlt.read_bus_voltage();

    state.sensors[DATA_TYPES::current_5V].value = curr5v.read_current();
    state.sensors[DATA_TYPES::current_3V].value = curr3v.read_current();
    state.sensors[DATA_TYPES::current_bat].value = currBatv.read_current();
    state.sensors[DATA_TYPES::cam_current].value = curr12VCam.read_current();
    state.sensors[DATA_TYPES::altimeter_current].value = curr12VAlt.read_current();

    // Send message over Serial port
    int  message_length;

    data_array[0] = state.sensors[DATA_TYPES::temp_int_hih].value;           // In degree Celsius
    data_array[1] = state.sensors[DATA_TYPES::temp_int_mpl].value;           // In degree Celsius
    data_array[2] = state.sensors[DATA_TYPES::temp_int_xsens].value;         // In degree Celsius
    data_array[3] = state.sensors[DATA_TYPES::temp_ext].value;               // In degree Celsius
    data_array[4] = state.sensors[DATA_TYPES::press_int].value;              // In Pascals
    data_array[5] = state.sensors[DATA_TYPES::humidity].value;               // In percent
    data_array[6] = state.sensors[DATA_TYPES::status].value;                 // Int
    data_array[7] = state.sensors[DATA_TYPES::pwm_ul].value;                 // Setpoint, between 0 and 1
    data_array[8] = state.sensors[DATA_TYPES::pwm_ur].value;                 // Setpoint, between 0 and 1
    data_array[9] = state.sensors[DATA_TYPES::pwm_fr].value;                 // Setpoint, between 0 and 1
    data_array[10] = state.sensors[DATA_TYPES::pwm_fl].value;                // Setpoint, between 0 and 1
    data_array[11] = state.sensors[DATA_TYPES::user_iface].value;            // Int
    
    char * message = protocol.createmsg(SerialProtocol::AD, id_data_array_packet_1, data_array,
                     12, &message_length);
    
    pc_serial_mutex.lock(); 
    for (int i = 0; i < message_length; i++) {
      pc.putc(message[i]);
    }

    data_array[0] = state.sensors[DATA_TYPES::current_3V].value;             // In mA
    data_array[1] =  state.sensors[DATA_TYPES::current_5V].value;            // In mA
    data_array[2] =  state.sensors[DATA_TYPES::current_bat].value;           // In mA
    data_array[3] =  state.sensors[DATA_TYPES::rbat_voltage].value;          // In V
    data_array[4] =  state.sensors[DATA_TYPES::r5V_voltage].value;           // In V
    data_array[5] =  state.sensors[DATA_TYPES::r3V_voltage].value;           // In V
    data_array[6] =  state.sensors[DATA_TYPES::latitude].value;              // In degrees
    data_array[7] =  state.sensors[DATA_TYPES::longitude].value;             // In degrees
    data_array[8] =  state.sensors[DATA_TYPES::cam_voltage].value;
    data_array[9] =  state.sensors[DATA_TYPES::altimeter_voltage].value;
    data_array[10] =  state.sensors[DATA_TYPES::cam_current].value;
    data_array[11] =  state.sensors[DATA_TYPES::altimeter_current].value;
    

    message = protocol.createmsg(SerialProtocol::AD, id_data_array_packet_2, data_array,
              12, &message_length);
    
    for (int i = 0; i < message_length; i++) {
      pc.putc(message[i]);
    }
    pc_serial_mutex.unlock();

    // Sensor heartbeat
    myled2 = !myled2;

    ThisThread::sleep_for(500);
  }
}

void commander_polling() {
    if (protocol.newMessagereceived()) {
      protocol.parse(protocol.lastMessage());
    }
}

// Callback for the receive serial interrupt of the pc port
void onPcRx() {
  while (pc.readable()) {
    protocol.process_char(pc.getc());
  }
}

#ifdef TARGET_BOARD
  void onVa500Rx() {
    while (va500_serial.readable()) {
      va500.addChar(va500_serial.getc());
    }
    va500_noData.attach(&noVa500Data, 2);
  }
#endif

void onGpsRx() {
  while (gps_serial.readable()) {
    gps.process_char(gps_serial.getc());
  }
  gps_noData.attach(&noGpsData, 4);
}

void onImuRx() {
  while (imu_serial.readable()) {
    char c = imu_serial.getc();
    imu.parseByte(c); 
  }
  imu_noData.attach(&noImuData, 2);
}

void count_ms() {
  ms_counter++;
  if (ms_counter==1000) ms_counter=0;
}

uint32_t onST(uint32_t myarg) {
  esc_ul.setThrottle(state.sensors[DATA_TYPES::pwm_ul].value);
  esc_ur.setThrottle(state.sensors[DATA_TYPES::pwm_ur].value);
  esc_fl.setThrottle(state.sensors[DATA_TYPES::pwm_fl].value);
  esc_fr.setThrottle(state.sensors[DATA_TYPES::pwm_fr].value);
  esc_ul.pulse();
  esc_ur.pulse();
  esc_fl.pulse();
  esc_fr.pulse();

  /*pc.printf("Got St! \n\r");
  pc.printf("UL: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_ul].value);
  pc.printf("UR: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_ur].value);
  pc.printf("FL: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_fl].value);
  pc.printf("FR: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_fr].value);*/
  // If no new message in 0.2 sec, stop thrusters.
  //state.sensors[DATA_TYPES::user_iface].value = oldiface;
  thruster_brownout.attach(&disableThrusters, 0.4);
  clearBit(status, NO_ST_DATA);
  return 0;
}

uint32_t onRL(uint32_t myarg) {
  pc.printf("Got RL for target %i \n\r", protocol.redirectId);

  int msg_len = strlen(protocol.lastMessage());
  pc.printf("String length is %i \n", msg_len);
  //i2c_mutex.lock();
  i2c.write(UI_uC_ADDRESS, protocol.lastMessage(), msg_len - 1);
  i2c.write(UI_uC_ADDRESS, "\n\r", 2);
  //i2c_mutex.unlock();
  // Clear the forwarding flag
  protocol.redirectId = 0;
  return 0;
}

uint32_t onRT(uint32_t myarg) {
  pc.printf("Got RT \n\r");
  return 0;
}

uint32_t onRD(uint32_t myarg) {
  pc.printf("Got RD \n\r");
  return 0;
}

uint32_t onRA(uint32_t myarg) {
  pc.printf("Got RA \n\r");
  return 0;
}

uint32_t onAK(uint32_t myarg) {
  pc.printf("Got AK for cmd %i  \n\r", static_cast<int>(myarg));
  return 0;
}

uint32_t onDM(uint32_t id) {
  int cmd_id = static_cast<int>(id);
  //pc.printf("Got DM for cmd %i  \n\r", cmd_id);
   
  pc_serial_mutex.lock();

  if (cmd_id==DATA_TYPES::current_time) {
      double integer, fraction; 
      fraction = modf(state.sensors[DATA_TYPES::current_time].value, &integer);
      //integer = 1; 
      set_time(integer);
      ms_counter = fraction * 1000;
      //pc.printf("Setting current time %f\n\r",state.sensors[DATA_TYPES::current_time].value);
  } else if (cmd_id==DATA_TYPES::cameraTriggerEnable) {
      pc.printf("Camera trigger: %5.2f\n\r", state.sensors[DATA_TYPES::cameraTriggerEnable].value);
  }
    pc_serial_mutex.unlock();

  return 0;
}

uint32_t onAD(uint32_t myarg) {
  pc.printf("Got AD ");

  for (int i = 0; i < DATA_TYPES::NumberOfTypes; i++) {
    pc.printf("Element ID: %i, Value: %8.2f \n\r", i, state.sensors[i].value);
  }
  return 0;
}

uint32_t onSS(uint32_t myarg) {
  pc.printf("Got SS: %f \n\r", state.sensors[DATA_TYPES::status].value);
  return 0;
}

uint32_t onUI(uint32_t myarg) {
  pc.printf("Got UI: %f \n\r", state.sensors[DATA_TYPES::user_iface].value);
  return 0;
}

void updateUI(void) {
  int i2c_result;
  int message_length;
  
  if (checkError(status, NO_ST_DATA)) {
    state.sensors[DATA_TYPES::user_iface].value = 7;
  }
  else{
    int numErrors = numSensorErrors(status);
    if (numErrors == 0) {
      state.sensors[DATA_TYPES::user_iface].value = 6; // No errors --> Green 
    } else if (numErrors == 1) {
      state.sensors[DATA_TYPES::user_iface].value = 2; // 1 error --> Blinky Green
    } else {
      state.sensors[DATA_TYPES::user_iface].value = 5; // 2+ errors --> Steady red
    }
  } 
  
  //reportErrors();

  char *message = protocol.createmsg(
        SerialProtocol::UI, state.sensors[DATA_TYPES::user_iface].value,
        &message_length);
  i2c_result = i2c.write(UI_uC_ADDRESS<<1, message, message_length - 1);
  i2c_result += i2c.write(UI_uC_ADDRESS<<1, "\n\r", 2);
  
  if (!i2c_result) {
    setBit(status, LEDUI_ERROR);
  } else {
    clearBit(status, LEDUI_ERROR);
  }

}

void resetCameraTrigger() {
  camera_trigger_pin = 0;
}

void getIMU(void) {
  
  // ********************* IMU Setup ******************************************
  imu_serial.attach(&onImuRx);
  imu_noData.attach(&noImuData, 2);

  // IMU Options: 
  ImuOptions imuoptions(true, false, false, false, false, false, 50, 50, 50, 1, XsFilter_MTI_10_VRUGeneral);
  
  pc_serial_mutex.lock();
	if(imu.initialize()){
    pc.printf("Succesfully initialized IMU\n\r");
  } else {
    pc.printf("Could not initialize IMU\n\r");
  }
	ThisThread::sleep_for(10);
	if (imu.configure(&imuoptions)) {
    pc.printf("Successfully configured IMU\n\r");
  } else {
    pc.printf("Could not configure IMU\n\r");
  }
	ThisThread::sleep_for(10);
  if (imu.streamMeasurements()) {
    pc.printf("Succesfully entered IMU streaming\n\r");
  } else {
    pc.printf("IMU did not ack streaming command\n\r");
  }
  ThisThread::sleep_for(10);
  pc_serial_mutex.unlock();

  // IMU to Camera Trigger Setup 
  int imuCounter = 0; 

  const int imu_msg_data_array[10] = {DATA_TYPES::imu_timestamp,
                              DATA_TYPES::roll, DATA_TYPES::pitch,
                              DATA_TYPES::yaw, DATA_TYPES::accel_x,
                              DATA_TYPES::accel_y, DATA_TYPES::accel_z,
                              DATA_TYPES::gyro_x, DATA_TYPES::gyro_y,
                              DATA_TYPES::gyro_z};

  //****************************** VA500 Setup  *******************************
  pc_serial_mutex.lock();
  pc.printf("IMU device ID: %X \n\r", imu.deviceId);


    #ifdef TARGET_BOARD
    va500_serial.attach(&onVa500Rx);
   
    wdt.kick();
    
    bool va_init = va500.setup();
    wdt.kick();
    if (!va_init) {
      pc.printf("Failed to initialize VA500RAP sensor\n\r");
    }
    bool va_freq = va500.setFrequency(4);
    if (!va_freq) {
      pc.printf("Failed to set frequency for VA500\n\r");
    }
    va500_noData.attach(&noVa500Data, 2);
   
  #endif

  pc_serial_mutex.unlock();

  wdt.kick();

  //****************************** Acquisition loop  *******************************

  while (true) {

    ThisThread::sleep_for(5);
    if (imu.newData) {
      imu.newData = false;
      clearBit(status, IMU_ERROR);
      //Get IMU timestamp 
      unsigned int seconds = (unsigned int) time(NULL);
      //pc.printf("MS counter: %i\n", ms_counter );
      double timestamp = (double) seconds + ((double)ms_counter)/1000; 

      // IMU to Camera Trigger 
      imuCounter++; 
      if ((imuCounter >= IMU_CAMERA_FREQ_DIVIDER) && (fabs(state.sensors[DATA_TYPES::cameraTriggerEnable].value -1) < 0.1)) {
        imuCounter=0; 
        cameraTriggerReset.attach(resetCameraTrigger, 0.004);
        camera_trigger_pin = 1; 
        pc.printf("Triggered!!!\n\r");
      }
    
      state.sensors[DATA_TYPES::roll].value = imu.euler[0];
      state.sensors[DATA_TYPES::pitch].value = imu.euler[1];
      state.sensors[DATA_TYPES::yaw].value = imu.euler[2];
      state.sensors[DATA_TYPES::accel_x].value = imu.accel[0];
      state.sensors[DATA_TYPES::accel_y].value = imu.accel[1];
      state.sensors[DATA_TYPES::accel_z].value = imu.accel[2];
      state.sensors[DATA_TYPES::gyro_x].value = imu.gyro[0];
      state.sensors[DATA_TYPES::gyro_y].value = imu.gyro[1];
      state.sensors[DATA_TYPES::gyro_z].value = imu.gyro[2];

      // Create and send message
      int message_length = 0;

      double data_array[10] = {timestamp,
                              state.sensors[DATA_TYPES::roll].value,
                              state.sensors[DATA_TYPES::pitch].value,
                              state.sensors[DATA_TYPES::yaw].value,
                              state.sensors[DATA_TYPES::accel_x].value,
                              state.sensors[DATA_TYPES::accel_y].value,
                              state.sensors[DATA_TYPES::accel_z].value,
                              state.sensors[DATA_TYPES::gyro_x].value,
                              state.sensors[DATA_TYPES::gyro_y].value,
                              state.sensors[DATA_TYPES::gyro_z].value };
      pc_serial_mutex.lock();

      char *message = protocol.createmsg(SerialProtocol::AD, imu_msg_data_array,
                                        data_array, 10, &message_length);
       for (int i = 0; i < message_length; i++) {
        pc.putc(message[i]);
      }
      pc_serial_mutex.unlock();

    } /* else {
      pc_serial_mutex.lock();
      pc.printf("Imu state: %i\n\r",imu.getState());
      pc_serial_mutex.unlock();
    }*/


     #ifdef TARGET_BOARD
    if (va500.newMessageReceived) {
      va500.newMessageReceived = false;
                
      if (va500.processBuffer()) {
        state.sensors[DATA_TYPES::press_ext].value = va500.getDepth() * 10000; // Convert dBar to Pa
        state.sensors[DATA_TYPES::altitude].value = va500.getAltitude(); 

        // Create and send message
        pc_serial_mutex.lock();
        int message_length = 0;
        int id_data_array[2] = {DATA_TYPES::altitude, DATA_TYPES::press_ext};
        double data_array[2] = {state.sensors[DATA_TYPES::altitude].value,
                               state.sensors[DATA_TYPES::press_ext].value};

        char *message = protocol.createmsg(SerialProtocol::AD, id_data_array,
                                           data_array, 2, &message_length);
        for (int i = 0; i < message_length; i++) {
          pc.putc(message[i]);
        }
        pc_serial_mutex.unlock();
      }    
    }   
    #endif 
  }
}

void heartbeat() {
  heartbeat_led = !heartbeat_led;
  wdt.kick();   
}


#ifndef MBED_TESTSUITE
int main() {
  pc_serial_mutex.lock();
  pc.printf("Starting Sphere LPC program!  \n\r");
  mbed_printf("MBED MINIMAL\n\r");
  if (wdt.check()) {
    printf("Brownout was triggered\n\r");
  }
  pc_serial_mutex.unlock();

  wdt.kick(6.0);  

  initialize_thrusters();

  

  // Initialization checks 
  ThisThread::sleep_for(500);  // Wait for sensors to initialize
  bool i2c_ok = TESTS::testI2Cbus(&i2c, &pc);
  state.sensors[DATA_TYPES::user_iface].value = (i2c_ok ? 6 : 3);
  
  


  // Start threads
  sensor_thread.start(sensor_polling);
  imu_thread.start(getIMU);

  ThisThread::sleep_for(100);
  mbed_mem_trace_set_callback(mbed_mem_trace_default_callback);
  rtos::Kernel::attach_idle_hook(TESTS::toggle_idle_pin); 

  // Initialize time 
  ms_ticker.attach(count_ms, 0.001);
  set_time(0);

  

  // Atach callbacks for the different received messages
  protocol.attach(SerialProtocol::ST, &onST);
  protocol.attach(SerialProtocol::RT, &onRT);
  protocol.attach(SerialProtocol::RD, &onRD);
  protocol.attach(SerialProtocol::RA, &onRA);
  protocol.attach(SerialProtocol::AK, &onAK);
  protocol.attach(SerialProtocol::DM, &onDM);
  protocol.attach(SerialProtocol::AD, &onAD);
  protocol.attach(SerialProtocol::SS, &onSS);
  protocol.attach(SerialProtocol::UI, &onUI);
  protocol.attach(SerialProtocol::RL, &onRL);

  // Atach the callback to the serial port receive method
  pc.attach(&onPcRx);
  gps_serial.attach(&onGpsRx);


  EventQueue *queue = mbed_event_queue();
  queue->call_every(100, commander_polling);
  queue->call_every(500, heartbeat);
  queue->call_every(1000, updateBLE);
  queue->call_every(500, updateUI);
  queue->call_every(5000, TESTS::printHeapUsage, &pc, &pc_serial_mutex);
  //queue->call_every(5000, TESTS::printAllThreadInfo, &pc, &pc_serial_mutex);
  queue->dispatch_forever();

}

#endif

