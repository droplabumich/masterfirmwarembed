#ifndef _DEFINES_H_INCLUDED 
#define _DEFINES_H_INCLUDED 

#ifdef TARGET_BOARD

#define GPS_RX P0_11
#define GPS_TX P0_10
#define PC_RX P0_3
#define PC_TX P0_2
#define EXT_RX P0_16
#define EXT_TX P0_15
#define I2C_SCL P0_20
#define I2C_SDA P0_19
#define LED1_p P1_16
#define LED2_p P1_17
#define XSENS_RX P0_1
#define XSENS_TX P0_0
#define PWM_UL_PIN P2_2
#define PWM_UR_PIN P2_1
#define PWM_FL_PIN P2_0
#define PWM_FR_PIN P2_3

#define DEBUG_PIN P2_13

#define MOSI_PIN P1_24
#define MISO_PIN P1_23
#define SCK_PIN P1_20
#define CS_PIN  P1_21

#elif DEV_BOARD

#define GPS_RX p10
#define GPS_TX p9
#define PC_RX USBRX
#define PC_TX USBTX
// #define EXT_RX
// #define EXT_TX
#define I2C_SCL p27
#define I2C_SDA p28
#define LED1_p LED1
#define LED2_p LED2
#define XSENS_RX p14
#define XSENS_TX p13
#define PWM_UL_PIN P2_2
#define PWM_UR_PIN P2_1
#define PWM_FL_PIN p24
#define PWM_FR_PIN P2_3
#define DEBUG_PIN p21

#define CS_PIN  LED3


#endif

#define BLE_NANO_ADDRESS 0x10
#define UI_uC_ADDRESS 0x22 //#define UI_uC_ADDRESS 0x44 
#define INA2195V_ADDRESS 0x41
#define INA2193V3_ADDRESS 0x44
#define INA219BatV_ADDRESS 0x45
#define HIH6130_ADDRESS 0x27
#define MPL3115_ADDRESS 0x60
 



#define MAX_MPL_RECONNECTIONS 10

#endif