
#ifndef _STATUS_H
#define _STATUS_H


#define HIH_ERROR 0 
#define MPL_ERROR 1 
#define GPS_ERROR 2 
#define IMU_ERROR 3
#define ALTIMETER_ERROR 4 
#define AVAILABLE 5 
#define INA5V_ERROR 6
#define INA3V3_ERROR 7 
#define INABAT_ERROR 8
#define AVAILABLE2 9 
#define LEDUI_ERROR 10
#define WATCHDOG_ERROR 11 
#define ODROID_COMMS_ERROR 12
#define ESC_TIMEOUT_ERROR 13 
#define VA500_NODATA_ERROR 14 
#define NO_ST_DATA 16



	void setBit(double *status, uint8_t errorCode) {
		uint64_t* status_uint64 = reinterpret_cast<uint64_t *> (status);
		*status_uint64 |= (uint64_t) 1 << errorCode;
	}


	void clearBit(double *status, uint8_t errorCode) {
		uint64_t* status_uint64 = reinterpret_cast<uint64_t *> (status);
		*status_uint64 &= ~((uint64_t) 1 << errorCode);
	}

	bool checkError(double *status, uint8_t errorCode) {
		uint64_t* status_uint64 = reinterpret_cast<uint64_t *> (status);
		return *status_uint64 & (((uint64_t) 1 << errorCode)) ?  true : false;
	}

	int numSensorErrors(double *status) {
		const uint8_t sensor2Check[] = {HIH_ERROR, MPL_ERROR, GPS_ERROR, IMU_ERROR, ALTIMETER_ERROR, INA5V_ERROR, INA3V3_ERROR, INABAT_ERROR};
		int numErrors = 0;
		for (int i=0; i<(sizeof(sensor2Check)/sizeof(uint8_t));i++) {
			if (checkError(status, sensor2Check[i])) 
				numErrors++;
		}
		return numErrors;
	}

	void setBit(uint64_t *status, uint8_t errorCode) {
		*status |= (uint64_t) 1 << errorCode;
	}


	void clearBit(uint64_t *status, uint8_t errorCode) {
		*status &= ~((uint64_t) 1 << errorCode);
	}

	bool checkError(uint64_t *status, uint8_t errorCode) {
		return *status & (((uint64_t) 1 << errorCode)) ?  true : false;
	}

	int numSensorErrors(uint64_t *status) {
		const uint8_t sensor2Check[] = {HIH_ERROR, MPL_ERROR, GPS_ERROR, IMU_ERROR, ALTIMETER_ERROR, INA5V_ERROR, INA3V3_ERROR, INABAT_ERROR};
		int numErrors = 0;
		for (int i=0; i<(sizeof(sensor2Check)/sizeof(uint8_t));i++) {
			if (checkError(status, sensor2Check[i])) {
				numErrors++;
			}
		}
		return numErrors;
	}



#endif