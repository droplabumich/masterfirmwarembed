#!/bin/sh
RED='\033[0;31m'
NC='\033[0m'
 
touch main.cpp
OPTIONS='-n tests-peripherals* -t GCC_ARM -m LPC1768'
 
printf "${RED}⇒ Building testsuite${NC}\n"
mbed test --compile ${OPTIONS} -DMBED_TESTSUITE=1 -DDEV_BOARD
 
printf "\n${RED}⇒ Running testsuite${NC}\n"
mbed test --run ${OPTIONS}

