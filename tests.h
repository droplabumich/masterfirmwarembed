#include "mbed.h"
#include "mbed_stats.h"
#include "mbed_mem_trace.h"

#include "ESC/esc.h"
#include "pinDefs.h"




#define TESTSTR_ST "$ST,1.2,233.1,0.431,-23*03\n"
#define TESTSTR_RD "$RD,1*0B\n"
#define TESTSTR_RA "$RA*13\n\r"
#define TESTSTR_AK "$AK,1*17\n"
#define TESTSTR_DM "$DM,1,233.1*15\n"
#define TESTSTR_AD                                                             \
  "$AD,0,31.100000,1,5.099999,9,14587.145507,10,-1.200000*20\n\r"
#define TESTSTR_SS "$SS,1,233.1*1C\n"
#define TESTSTR_UI "$UI,1*01\n"

#define MAX_THREAD_STATS    0x8

#if !defined(MBED_THREAD_STATS_ENABLED)
#error "Stats not enabled"
#endif

extern DigitalOut debug_pin;


namespace TESTS {
typedef struct {
    const char *strVal;  // or char strVal[20];
    int intVal;
} tTuple;

bool testI2Cbus(I2C *bus, RawSerial* serialIface) {
  const tTuple i2c_devices[] = {{"INA2195V", INA2195V_ADDRESS},   {"INA2193V3", INA2193V3_ADDRESS},
                          {"INA219BatV", INA219BatV_ADDRESS}, {"HIH6130", HIH6130_ADDRESS},
                          {"MPL3115A2", MPL3115_ADDRESS},  {"LED_UI", 0x22}};

  const tTuple *ptr = i2c_devices;
  const tTuple *endPtr = i2c_devices + sizeof(i2c_devices) / sizeof(i2c_devices[0]);

  bool success = true;
  serialIface->printf("Testing I2C bus devices:\r\n");
  while (ptr < endPtr) {
    bus->start();
    if (!bus->write((*ptr).intVal << 1)) {
        serialIface->printf("%s with address 0x%X did not respond in I2C bus.\n",
             (*ptr).strVal, (*ptr).intVal);
        success = false;
    }
    bus->stop();
    wait_ms(10);
    ptr++;
  }
  serialIface->printf("Finished\r\n");
  return success;
}

bool test_thruster(ESC *thruster) {
  float th = 0;
  thruster->setThrottle(0.5);
  thruster->pulse();
  ThisThread::sleep_for(500);
  while (th < 1) {
    th += 0.01;
    thruster->setThrottle(th);
    thruster->pulse();
    ThisThread::sleep_for(500);
  }
  thruster->setThrottle(0.5);
  thruster->pulse();
  return true;
}

void i2cScanner(I2C* i2cbus, RawSerial* serialIface) {
  serialIface->printf("RUN\r\n");
  for (int i = 0; i < 128; i++) {
    i2cbus->start();
    if (i2cbus->write(i << 1))
      serialIface->printf("0x%x ACK \r\n", i); // Send command string
    i2cbus->stop();
  }
}

static void printAllThreadInfo(RawSerial* serialIface, Mutex* serialMutex) {
    serialMutex->lock();
    serialIface->printf("Thread Info: \n");
    mbed_stats_thread_t *stats = new mbed_stats_thread_t[MAX_THREAD_STATS];
    int count = mbed_stats_thread_get_each(stats, MAX_THREAD_STATS);
    
    for(int i = 0; i < count; i++) {
        serialIface->printf("ID: 0x%x \n\r", stats[i].id);
        serialIface->printf("Name: %s \n\r", stats[i].name);
        serialIface->printf("State: %d \n\r", stats[i].state);
        serialIface->printf("Priority: %d \n\r", stats[i].priority);
        serialIface->printf("Stack Size: %d \n\r", stats[i].stack_size);
        serialIface->printf("Stack Space: %d \n\r", stats[i].stack_space);
        serialIface->printf("\n");
    }
    serialMutex->unlock();
    free(stats);
    return;
}

static void printHeapUsage(RawSerial* serialIface, Mutex* serialMutex) {
  mbed_stats_heap_t heap_stats;
  mbed_stats_heap_get(&heap_stats);
  serialMutex->lock();
  serialIface->printf("Heap size: %lu / %lu bytes\r\n", heap_stats.current_size, heap_stats.reserved_size);
  serialMutex->unlock();
}



void toggle_idle_pin() {
  debug_pin = !debug_pin;
}

}